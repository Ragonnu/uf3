﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class moverConRaton : MonoBehaviour
{
    private NavMeshAgent navMess;
    public Camera cam;
    public Transform target;
    // Start is called before the first frame update
    void Start()
    {
        navMess = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        //this.GetComponent<UnityEngine.AI.NavMeshAgent>().destination = target.position;

        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = cam.ScreenPointToRay(Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit))
            {
                navMess.SetDestination(hit.point);
            }

        }
    }

}
