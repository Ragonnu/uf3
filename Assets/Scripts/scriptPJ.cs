﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class scriptPJ : MonoBehaviour
{
    CharacterController cc;
    public float speed;
    public float jumpSpeed = 15.0F;
    public float gravity = 30.0F;
    private Vector3 moveDirection = Vector3.zero;
    public GameEvent muerte;

    Vector2 currentMouseLook;
    Vector2 appliedMouseDelta;
    public float sensitivity = 2;
    public float smoothing = 2;
    // Start is called before the first frame update
    void Start()
    {
        cc = this.GetComponent<CharacterController>();
    }

    // Update is called once per frame
    public void Update()
    {

        float ver = Input.GetAxis("Vertical");
        float hor = Input.GetAxis("Horizontal");
        Vector3 movement = transform.forward * ver + transform.right * hor;
        cc.SimpleMove(speed * Time.deltaTime * movement);

        if (cc.isGrounded && Input.GetButton("Jump"))
        {
            moveDirection.y = jumpSpeed;
        }
        moveDirection.y -= gravity * Time.deltaTime;
        cc.Move(moveDirection * Time.deltaTime);


        //Si se necessita rotar al personaje con el raton
        rotate();
    }

    private void rotate()
    {
        Vector2 smoothMouseDelta = Vector2.Scale(new Vector2(Input.GetAxisRaw("Mouse X"), Input.GetAxisRaw("Mouse Y")), Vector2.one * sensitivity * smoothing);
        appliedMouseDelta = Vector2.Lerp(appliedMouseDelta, smoothMouseDelta, 1 / smoothing);
        currentMouseLook += appliedMouseDelta;
        currentMouseLook.y = Mathf.Clamp(currentMouseLook.y, -90, 90);

        // Rotate camera and controller.
        transform.localRotation = Quaternion.AngleAxis(-currentMouseLook.y, Vector3.right);
        transform.localRotation = Quaternion.AngleAxis(currentMouseLook.x, Vector3.up);
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.tag == "cubo")
        {
            muerte.Raise();
        }
    }
}

